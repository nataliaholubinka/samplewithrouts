package com.upRegattas.updateParticipantCoordinatesAndBattery;

import com.google.gson.annotations.SerializedName;


public class requestPartisCoordinates {
    @SerializedName("game_id")
    int mGame_id;

    @SerializedName("command_id")
    int mCommand_id;

    @SerializedName("lat")
    String mLat;

    @SerializedName("lng")
    String mLng;

    @SerializedName("battery_status")
    String mBattery_status;

    public void setmGame_id(int mGame_id) {
        this.mGame_id = mGame_id;
    }

    public void setmCommand_id(int mCommand_id) {
        this.mCommand_id = mCommand_id;
    }

    public void setmLat(String mLat) {
        this.mLat = mLat;
    }

    public void setmLng(String mLng) {
        this.mLng = mLng;
    }

    public void setmBattery_status(String mBattery_status) {
        this.mBattery_status = mBattery_status;
    }
}
