package com.service;

import com.Variables;
import com.google.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;

/**
 * Created by Alex on 07.05.2017.
 */
public class MultiThreadParticipantPositionUpdate implements Runnable{
    ApiManager apiManager = new ApiManager();
    Variables variables = new Variables();
    int mCommand_id;
    int mGame_id;
    String mLat;
    String mLng;
    String mBattery_status;
    String mToken;
    List<LatLng> mListPoints = new ArrayList<LatLng>();

    public MultiThreadParticipantPositionUpdate(List<LatLng> listPoints, int command_id , int game_id , String battery_status, String token){
        this.mCommand_id = command_id;
        this.mGame_id = game_id;
        this.mBattery_status = battery_status;
        this.mToken = token;
        this.mListPoints = listPoints;
    }



    public void run() {
        for(int i = 0; i < mListPoints.size(); i++) {
            Random random = new Random();
            try {
                sleep(random.nextInt(Variables.ROUND_TIME_STEP));
                System.out.println("idThread: " + Thread.currentThread().getId() + "   Command_id: " + mCommand_id + "   Game_id: " + mGame_id);
                apiManager.updateParticipantCoordinates(mCommand_id, mGame_id, mListPoints.get(i).lat, mListPoints.get(i).lng, mBattery_status, mToken);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
