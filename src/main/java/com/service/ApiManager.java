package com.service;

import com.Variables;
import com.entity.apRegattaMap.getRegatta.responseRegatta;
import com.connection.CRequest;
import com.connection.CResult;
import com.connection.ServerConnection;
import com.google.gson.reflect.TypeToken;
import com.google.maps.model.LatLng;
import com.upRegattas.updateParticipantCoordinatesAndBattery.requestPartisCoordinates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natalia Holubinka on 05.05.2017.
 */
public class ApiManager {
    public List<LatLng> getRegata (int id){
        CRequest mRequest = new CRequest();
        CResult result = new CResult();
        Variables variables = new Variables();

        mRequest.mURL = variables.getUrl() + variables.getUrlGetRegata() + id ;
        ServerConnection serverConnection = new ServerConnection(mRequest, ServerConnection.FMethod.GET, null);
        serverConnection.setResponseType(new TypeToken<responseRegatta>() {}.getType(), false);
        result = serverConnection.connect();
        responseRegatta mResponseRegatta = (responseRegatta)result.ObjResult;
        List<LatLng> resultList = new ArrayList<LatLng>();

        for (int i = 0; i < mResponseRegatta.getmPoints().size(); i++){
            LatLng latLng = new LatLng( Double.valueOf(mResponseRegatta.getmPoints().get(i).getmLat()),  Double.valueOf(mResponseRegatta.getmPoints().get(i).getmLng()));
            resultList.add(latLng);
        }

        return resultList;
    }

    public void updateParticipantCoordinates (int command_id , int game_id , Double lat , Double lng, String battery_status, String token){
        CRequest mRequest = new CRequest();
        CResult result = new CResult();
        requestPartisCoordinates request = new requestPartisCoordinates();
        request.setmCommand_id(command_id);
        request.setmGame_id(game_id);
        request.setmLat(lat.toString());
        request.setmLng(lng.toString());
        request.setmBattery_status(battery_status);
        Variables variables = new Variables();

        mRequest.mURL = variables.getUrl() + variables.getUrlSetParticipantCoordinates();
        mRequest.ObjRequest = request;
        ServerConnection serverConnection = new ServerConnection(mRequest, ServerConnection.FMethod.PUT, token);
        serverConnection.connect();
    }

}
