package com;


import com.connection.CResult;
import com.google.maps.model.LatLng;
import com.ppAuthorization.LoginRegattaParticipant;
import com.regatta.GetActiveRegattaOfCurrentParticipant;
import com.service.ApiManager;
import com.service.GeoDataParser;
import com.service.MultiThreadParticipantPositionUpdate;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        CResult cResult = new CResult();
        ApiManager apiManager = new ApiManager();
        GeoDataParser geoDataParser = new GeoDataParser();
        Variables variables = new Variables();
        List<LatLng> latLngList = new ArrayList<LatLng>();
        LoginRegattaParticipant loginRegattaParticipant = new LoginRegattaParticipant();
        List<String> tokenList = new ArrayList<String>();
        Thread Participant = null;
        List<Integer> regataList = new ArrayList<Integer>();
        GetActiveRegattaOfCurrentParticipant getActiveRegattaOfCurrentParticipant = new GetActiveRegattaOfCurrentParticipant();


        //Get token
        for(int i = 0; i < variables.getIdentityList().size(); i++ ){
            tokenList.add(loginRegattaParticipant.getParticipantToken(variables.getIdentityList().get(i).toString(), variables.password));
        //Get game_id
            regataList.add(getActiveRegattaOfCurrentParticipant.GetActiveRegattaOfCurrentParticipant(tokenList.get(i)));
        }

        while(true){
            //Run update Participant position in different threads
            for(int i = 0; i < variables.getIdentityList().size(); i++ ){
                latLngList =  geoDataParser.getRouteMidlePoints(apiManager.getRegata(regataList.get(i)));
                Participant = new Thread(new MultiThreadParticipantPositionUpdate(latLngList, variables.getCommand_id()[i], regataList.get(i), "50%", tokenList.get(i)));
                Participant.start();
            }
            try {
                Participant.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
