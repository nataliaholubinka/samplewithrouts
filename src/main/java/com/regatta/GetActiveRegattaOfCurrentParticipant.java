package com.regatta;

import com.Variables;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class GetActiveRegattaOfCurrentParticipant {

    public Integer GetActiveRegattaOfCurrentParticipant(String token) {
        Variables variables = new Variables();
        String url = variables.getUrl();
        String urlRegatta = variables.getUrlRegatta();

        Response get;
        get = given().
                header("Content-Type", "application/json").
                header("Authorization", ("Bearer " + token)).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlRegatta).
                then().
                extract().
                response();

////        GET REGATTA ID FROM RESPONSE
        int tripId = get.path("data[0].id");
//        System.out.println("Regatta Id " + tripId);
        return tripId;
    }

    public GetActiveRegattaOfCurrentParticipant() {
    }
}