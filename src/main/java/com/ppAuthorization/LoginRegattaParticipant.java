package com.ppAuthorization;

import com.Variables;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class LoginRegattaParticipant {
    public LoginRegattaParticipant() {
    }

    public String getParticipantToken(String email, String password) {
        Variables variables = new Variables();
        String url = variables.getUrl();
        String urlLogin = variables.getUrlParticipantLogin();

        System.out.println("Email: " + email);

        Response loginPost;
        loginPost = given().
                relaxedHTTPSValidation().
                param("identity", email).
                param("password", password).
                expect().
                statusCode(200).
//                log().all().
                when().
                post(url + urlLogin).
                then().
                extract().
                response();

////        GET TOKEN FROM RESPONSE
        String loginToken = loginPost.path("data.token.token");
//        System.out.println("LoginToken: " + loginToken);
        return loginToken;
    }
}