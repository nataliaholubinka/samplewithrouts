package com;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Variables {
    List<String> identityList = new ArrayList<String>(Arrays.asList("test1@example.com", "test2@example.com", "test3@example.com"));
    String url = "http://setsail.demo.relevant.software";
    String urlParticipantLogin = "/api/v1/auth/login";
    String urlRegatta = "/api/v1/regatta/active";
    String urlGetRegata = "/api/v1/regatta/";
    String urlSetParticipantCoordinates = "/api/v1/regatta/set-participant-coordinates";
    int [] command_id = {5, 6, 7};
    String password = "12345678";
    public static final double MIN_STEP = 350;  //мінімальна відстань між точками
    public static final int ROUND_TIME_STEP = 3000;  //мах час в мс, між звертаннями на сервер(вибирається рандомно)


    public String getUrl() {
        return url;
    }

    public String getUrlParticipantLogin() {
        return urlParticipantLogin;
    }
    public void setIdentityList(List<String> identityList) {
        this.identityList = identityList;
    }

    public int[] getCommand_id() {
        return command_id;
    }

    public void setCommand_id(int[] command_id) {
        this.command_id = command_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Variables()
    {
    }

    public List<String> getIdentityList() {
        return identityList;
    }

    public String getPassword() {
        return password;
    }

    public String getUrlRegatta() {
        return urlRegatta;
    }

    public String getUrlGetRegata() {
        return urlGetRegata;
    }

    public String getUrlSetParticipantCoordinates() {
        return urlSetParticipantCoordinates;
    }
}
