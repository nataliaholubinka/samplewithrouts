package com.entity.apRegattaMap.getRegatta;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class responseRegatta {
    @SerializedName("id")
    String mId;

    @SerializedName("title")
    String mTitle;

    @SerializedName("body")
    String mBody;

    @SerializedName("address")
    String mAddress;

    @SerializedName("status")
    String mStatus;

    @SerializedName("city")
    String mCitys;

    @SerializedName("is_circle")
    String mIs_circle;

    @SerializedName("started_at")
    String mStarted_at;

    @SerializedName("finished_at")
    String mFinished_at;

    @SerializedName("country")
    String mCountry;

    @SerializedName("points")
    List<points> mPoints;

    public String getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmBody() {
        return mBody;
    }

    public String getmAddress() {
        return mAddress;
    }

    public String getmStatus() {
        return mStatus;
    }

    public String getmCitys() {
        return mCitys;
    }

    public String getmIs_circle() {
        return mIs_circle;
    }

    public String getmStarted_at() {
        return mStarted_at;
    }

    public String getmFinished_at() {
        return mFinished_at;
    }

    public String getmCountry() {
        return mCountry;
    }

    public List<points> getmPoints() {
        return mPoints;
    }
}
