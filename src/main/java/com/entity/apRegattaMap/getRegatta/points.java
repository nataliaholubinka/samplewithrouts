package com.entity.apRegattaMap.getRegatta;


import com.google.gson.annotations.SerializedName;

public class points {
    @SerializedName("id")
    String mId;

    @SerializedName("lat")
    String mLat;

    @SerializedName("lng")
    String mLng;

    @SerializedName("id_game")
    String mId_game;

    @SerializedName("position")
    String mPosition;

    public String getmId() {
        return mId;
    }

    public String getmLat() {
        return mLat;
    }

    public String getmLng() {
        return mLng;
    }

    public String getmId_game() {
        return mId_game;
    }

    public String getmPosition() {
        return mPosition;
    }
}
